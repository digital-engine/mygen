package com.gitee.mygen.impl;

import com.gitee.mygen.config.MygenEnv;
import com.gitee.mygen.utils.NameUtils;
import lombok.AllArgsConstructor;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.CommentGeneratorConfiguration;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.Context;
import org.mybatis.generator.config.JDBCConnectionConfiguration;
import org.mybatis.generator.config.JavaClientGeneratorConfiguration;
import org.mybatis.generator.config.JavaModelGeneratorConfiguration;
import org.mybatis.generator.config.JavaTypeResolverConfiguration;
import org.mybatis.generator.config.ModelType;
import org.mybatis.generator.config.PluginConfiguration;
import org.mybatis.generator.config.SqlMapGeneratorConfiguration;
import org.mybatis.generator.config.TableConfiguration;
import org.mybatis.generator.internal.DefaultShellCallback;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
public class FileGenerator {

    private final MygenEnv mygenEnv;
    private final List<PluginConfiguration> pluginConfigs;
    private final JDBCConnectionConfiguration jdbcConnectionConfig;
    private final CommentGeneratorConfiguration commentGeneratorConfig;
    private final JavaTypeResolverConfiguration JavaTypeResolverConfig;
    private final JavaModelGeneratorConfiguration javaModelGeneratorConfig;
    private final JavaClientGeneratorConfiguration javaClientGeneratorConfig;
    private final SqlMapGeneratorConfiguration sqlMapGeneratorConfig;

    public void generateFiles() throws Exception {
        Configuration configuration = parseConfig();
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(configuration, new DefaultShellCallback(true), new ArrayList<>());
        myBatisGenerator.generate(null);
    }

    private Configuration parseConfig() {
        // 上下文
        // hierarchical 生成多个POJO，之间为继承关系
        // flat 生成单个POJO
        // conditional 根据条件生成单个或多个POJO
        Context context = new Context(ModelType.FLAT);
        context.setId("DEFAULT");
        context.setTargetRuntime("MyBatis3");
        // 插件
        pluginConfigs.forEach(context::addPluginConfiguration);
        // 数据库
        context.setJdbcConnectionConfiguration(jdbcConnectionConfig);
        // 注释
        context.setCommentGeneratorConfiguration(commentGeneratorConfig);
        // 类型映射
        context.setJavaTypeResolverConfiguration(JavaTypeResolverConfig);
        // 持久化对象
        context.setJavaModelGeneratorConfiguration(javaModelGeneratorConfig);
        // mapper接口
        context.setJavaClientGeneratorConfiguration(javaClientGeneratorConfig);
        // mapper.xml
        context.setSqlMapGeneratorConfiguration(sqlMapGeneratorConfig);
        // 表
        for (String tableName : mygenEnv.getTableNames()) {
            TableConfiguration tableConfig = new TableConfiguration(context);
            tableConfig.setTableName(tableName);
            tableConfig.setDomainObjectName(NameUtils.getTypeName(tableName));
            tableConfig.setCountByExampleStatementEnabled(false);
            tableConfig.setUpdateByExampleStatementEnabled(false);
            tableConfig.setDeleteByExampleStatementEnabled(false);
            tableConfig.setSelectByExampleStatementEnabled(false);
            context.addTableConfiguration(tableConfig);
        }
        // 整体配置
        Configuration configuration = new Configuration();
        configuration.addContext(context);
        return configuration;
    }

}
