package com.gitee.mygen.impl;

import com.gitee.mygen.utils.FileUtils;
import lombok.Data;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Data
public class ModuleScanner {

    private String projectPath;
    private String modulePath;
    private String sourcePath;
    private String resourcesPath;
    private Map<String, Directory> directoryMap = new LinkedHashMap<>();

    public ModuleScanner(String projectPath, String modulePath) {
        this.projectPath = projectPath;
        this.modulePath = modulePath;
        this.sourcePath = modulePath + "\\src\\main\\java";
        this.resourcesPath = modulePath + "\\src\\main\\resources";
    }

    public void scan(String... dirNames) {
        List<File> files = FileUtils.collectDir(sourcePath, dirNames);
        for (File file : files) {
            Directory directory = newDirectory(sourcePath, file);
            directoryMap.putIfAbsent(directory.getName(), directory);
        }
    }

    private Directory newDirectory(String sourcePath, File file) {
        Directory directory = new Directory();
        directory.setFile(file);
        directory.setName(file.getName());
        directory.setPackageStr(FileUtils.getPackage(sourcePath, file));
        return directory;
    }

    public Directory getDirectory(String name) {
        return directoryMap.get(name);
    }

    public String getPackage(String name) {
        Directory directory = directoryMap.get(name);
        return directory != null ? directory.getPackageStr() : null;
    }

    @Data
    public static class Directory {
        private File file;
        private String name;
        private String packageStr;
    }

}
