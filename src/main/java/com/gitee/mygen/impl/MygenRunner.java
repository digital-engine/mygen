package com.gitee.mygen.impl;

import lombok.AllArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class MygenRunner implements ApplicationRunner {

    private final FileGenerator fileGenerator;
    private final FileReplicator fileReplicator;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        fileGenerator.generateFiles();
        fileReplicator.copyFiles();
    }

}
