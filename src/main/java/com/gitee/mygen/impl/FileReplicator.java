package com.gitee.mygen.impl;

import cn.hutool.core.io.FileUtil;
import com.gitee.mygen.config.MygenEnv;
import com.gitee.mygen.utils.NameUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@AllArgsConstructor
public class FileReplicator {

    private static final int UNKNOWN_STATE = 0;
    private static final int COPY_STATE = 1;
    private static final int OVERWRITE_STATE = 2;

    private final MygenEnv mygenEnv;

    public void copyFiles() {
        List<String> tableNames = mygenEnv.getTableNames();
        for (String tableName : tableNames) {
            String typeName = NameUtils.getTypeName(tableName);
            mergePojo(typeName);
            mergeMapperInterface(typeName);
            mergeMapperXml(typeName);
        }
    }

    private void mergePojo(String typeName) {
        String mergeMode = mygenEnv.getMergeMode();
        String sourcePath = mygenEnv.getSourcePath();
        String targetSourcePath = mygenEnv.getTargetSourcePath();
        String pojoPackage = mygenEnv.getPojoPackage();

        String pojoPath = NameUtils.getJavaFilePath(sourcePath, pojoPackage, typeName);
        String targetPojoPath = NameUtils.getJavaFilePath(targetSourcePath, pojoPackage, typeName);
        int state = determine(mergeMode, targetPojoPath);
        if (state == COPY_STATE) {
            FileUtil.copy(pojoPath, targetPojoPath, false);
            log.info("Copy pojo! from: {}, to: {}", pojoPath, targetPojoPath);

        } else if (state == OVERWRITE_STATE) {
            FileUtil.copy(pojoPath, targetPojoPath, true);
            log.info("Overwrite pojo! from: {}, to: {}", pojoPath, targetPojoPath);
        }
    }

    private void mergeMapperInterface(String typeName) {
        String mergeMode = mygenEnv.getMergeMode();
        String sourcePath = mygenEnv.getSourcePath();
        String targetSourcePath = mygenEnv.getTargetSourcePath();
        String mapperPackage = mygenEnv.getMapperPackage();

        String mapperInterfacePath = NameUtils.getJavaFilePath(sourcePath, mapperPackage, typeName + "Mapper");
        String targetMapperInterfacePath = NameUtils.getJavaFilePath(targetSourcePath, mapperPackage, typeName + "Mapper");
        int state = determine(mergeMode, targetMapperInterfacePath);
        if (state == COPY_STATE) {
            FileUtil.copy(mapperInterfacePath, targetMapperInterfacePath, false);
            log.info("Copy mapper interface! from: {}, to: {}", mapperInterfacePath, targetMapperInterfacePath);

        } else if (state == OVERWRITE_STATE) {
            FileUtil.copy(mapperInterfacePath, targetMapperInterfacePath, true);
            log.info("Overwrite mapper interface! from: {}, to: {}", mapperInterfacePath, targetMapperInterfacePath);
        }
    }

    private void mergeMapperXml(String typeName) {
        String mergeMode = mygenEnv.getMergeMode();
        String resourcesMapperPath = mygenEnv.getResourcesMapperPath();
        String targetResourcesMapperPath = mygenEnv.getTargetResourcesMapperPath();

        String mapperXmlPath = NameUtils.getXmlFilePath(resourcesMapperPath, typeName + "Mapper");
        String targetMapperXmlPath = NameUtils.getXmlFilePath(targetResourcesMapperPath, typeName + "Mapper");
        int state = determine(mergeMode, targetMapperXmlPath);
        if (state == COPY_STATE) {
            FileUtil.copy(mapperXmlPath, targetMapperXmlPath, false);
            log.info("Copy mapper xml! from: {}, to: {}", mapperXmlPath, targetMapperXmlPath);

        } else if (state == OVERWRITE_STATE) {
            FileUtil.copy(mapperXmlPath, targetMapperXmlPath, true);
            log.info("Overwrite mapper xml! from: {}, to: {}", mapperXmlPath, targetMapperXmlPath);
        }
    }

    private int determine(String mergeMode, String filePath) {
        if ("copy".equals(mergeMode) && !FileUtil.exist(filePath)) {
            return COPY_STATE;
        } else if ("overwrite".equals(mergeMode)) {
            return OVERWRITE_STATE;
        }
        return UNKNOWN_STATE;
    }

}
