package com.gitee.mygen.utils;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FileUtils {

    public static List<File> collectDir(String path, String... dirNames) {
        List<File> result = new ArrayList<>();
        List<File> files = Collections.singletonList(FileUtil.newFile(path));
        Set<String> dirNamesSet = new HashSet<>(Arrays.asList(dirNames));
        collectDir(result, files, dirNamesSet);
        return result;
    }

    public static void collectDir(List<File> result, List<File> files, Set<String> dirNames) {
        for (File file : files) {
            if (file.isDirectory()) {
                if (dirNames.contains(file.getName())) {
                    result.add(file);
                }
                File[] childFiles = file.listFiles();
                if (childFiles != null) {
                    collectDir(result, Arrays.asList(childFiles), dirNames);
                }
            }
        }
    }

    public static String getPackage(String sourcePath, File file) {
        String absolutePath = file.getAbsolutePath();
        if (absolutePath.startsWith(sourcePath)) {
            String path = StrUtil.removePrefix(absolutePath, sourcePath + "\\");
            path = StrUtil.replace(path, "\\", ".");
            return path.endsWith(".") || path.endsWith(".java") ? path.substring(0, path.lastIndexOf('.')) : path;
        }
        return null;
    }

}
