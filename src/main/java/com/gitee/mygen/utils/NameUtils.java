package com.gitee.mygen.utils;

import cn.hutool.core.util.StrUtil;

import java.io.File;

public class NameUtils {

    public static String getTypeName(String tableName) {
        String typeName = StrUtil.toCamelCase(tableName);
        typeName = typeName.substring(0, 1).toUpperCase() + typeName.substring(1);
        return typeName;
    }

    public static String getJavaFilePath(String srcPath, String packageStr, String typeName) {
        String packagePath = StrUtil.replace(packageStr, ".", File.separator);
        return srcPath + File.separator + packagePath + File.separator + typeName + ".java";
    }

    public static String getXmlFilePath(String dirPath, String typeName) {
        return dirPath + File.separator + typeName + ".xml";
    }

}
