package com.gitee.mygen.config;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "spring.datasource")
public class DatasourceEnv {

    private String url;
    private String username;
    private String password;
    private String driverClassName;

    /**
     * tinyInt1isBit参数防止tinyint映射到Boolean类型
     * useInformationSchema让jdbc可以查询到表名等额外信息
     */
    public void setUrl(String url) {
        if (StringUtils.isNotBlank(url)) {
            if (!url.contains("tinyInt1isBit")) {
                url += "&tinyInt1isBit=false";
            }
            if (!url.contains("useInformationSchema")) {
                url += "&useInformationSchema=true";
            }
        }
        this.url = url;
    }

}
