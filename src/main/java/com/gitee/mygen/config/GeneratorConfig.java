package com.gitee.mygen.config;

import com.gitee.mygen.plugin.JavaTypeResolver;
import com.gitee.mygen.plugin.MapperInterfacePlugin;
import com.gitee.mygen.plugin.MapperXmlPlugin;
import com.gitee.mygen.plugin.pojo.JPAPlugin;
import com.gitee.mygen.plugin.pojo.LombokPlugin;
import com.gitee.mygen.plugin.pojo.MybatisPlusPlugin;
import lombok.AllArgsConstructor;
import org.mybatis.generator.config.CommentGeneratorConfiguration;
import org.mybatis.generator.config.JDBCConnectionConfiguration;
import org.mybatis.generator.config.JavaClientGeneratorConfiguration;
import org.mybatis.generator.config.JavaModelGeneratorConfiguration;
import org.mybatis.generator.config.JavaTypeResolverConfiguration;
import org.mybatis.generator.config.PluginConfiguration;
import org.mybatis.generator.config.SqlMapGeneratorConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class GeneratorConfig {

    private final DatasourceEnv datasourceEnv;
    private final MygenEnv mygenEnv;

    @Bean
    public PluginConfiguration lombokPluginConfig() {
        PluginConfiguration pluginConfig = new PluginConfiguration();
        pluginConfig.setConfigurationType(LombokPlugin.class.getName());
        return pluginConfig;
    }

    @Bean
    @ConditionalOnProperty(name = "mygen.plugin", havingValue = "mybatis-plus")
    public PluginConfiguration mybatisPlusPluginConfig() {
        PluginConfiguration pluginConfig = new PluginConfiguration();
        pluginConfig.setConfigurationType(MybatisPlusPlugin.class.getName());
        return pluginConfig;
    }

    @Bean
    @ConditionalOnProperty(name = "mygen.plugin", havingValue = "tk-mybatis")
    public PluginConfiguration jpaPluginConfig() {
        PluginConfiguration pluginConfig = new PluginConfiguration();
        pluginConfig.setConfigurationType(JPAPlugin.class.getName());
        return pluginConfig;
    }

    @Bean
    public PluginConfiguration mapperInterfacePluginConfig() {
        PluginConfiguration pluginConfig = new PluginConfiguration();
        pluginConfig.setConfigurationType(MapperInterfacePlugin.class.getName());
        return pluginConfig;
    }

    @Bean
    public PluginConfiguration mapperXmlPluginConfig() {
        PluginConfiguration pluginConfig = new PluginConfiguration();
        pluginConfig.setConfigurationType(MapperXmlPlugin.class.getName());
        return pluginConfig;
    }

    @Bean
    public JDBCConnectionConfiguration jdbcConnectionConfig() {
        JDBCConnectionConfiguration jdbcConnectionConfig = new JDBCConnectionConfiguration();
        jdbcConnectionConfig.setConnectionURL(datasourceEnv.getUrl());
        jdbcConnectionConfig.setUserId(datasourceEnv.getUsername());
        jdbcConnectionConfig.setPassword(datasourceEnv.getPassword());
        jdbcConnectionConfig.setDriverClass(datasourceEnv.getDriverClassName());
        return jdbcConnectionConfig;
    }

    @Bean
    public CommentGeneratorConfiguration commentGeneratorConfig() {
        CommentGeneratorConfiguration commentGeneratorConfig = new CommentGeneratorConfiguration();
        commentGeneratorConfig.addProperty("suppressDate", "false");
        commentGeneratorConfig.addProperty("suppressAllComments", "true");
        return commentGeneratorConfig;
    }

    @Bean
    public JavaTypeResolverConfiguration JavaTypeResolverConfig() {
        JavaTypeResolverConfiguration JavaTypeResolverConfig = new JavaTypeResolverConfiguration();
        JavaTypeResolverConfig.setConfigurationType(JavaTypeResolver.class.getName());
        JavaTypeResolverConfig.addProperty("forceBigDecimals", "false");
        return JavaTypeResolverConfig;
    }

    @Bean
    public JavaModelGeneratorConfiguration javaModelGeneratorConfig() {
        JavaModelGeneratorConfiguration javaModelGeneratorConfig = new JavaModelGeneratorConfiguration();
        javaModelGeneratorConfig.setTargetProject(mygenEnv.getSourcePath());
        javaModelGeneratorConfig.setTargetPackage(mygenEnv.getPojoPackage());
        javaModelGeneratorConfig.addProperty("enableSubPackages", "false");
        javaModelGeneratorConfig.addProperty("constructorBased", "false");
        javaModelGeneratorConfig.addProperty("immutable", "false");
        javaModelGeneratorConfig.addProperty("trimStrings", "false");
        return javaModelGeneratorConfig;
    }

    @Bean
    public JavaClientGeneratorConfiguration javaClientGeneratorConfig() {
        JavaClientGeneratorConfiguration javaClientGeneratorConfig = new JavaClientGeneratorConfiguration();
        javaClientGeneratorConfig.setTargetProject(mygenEnv.getSourcePath());
        javaClientGeneratorConfig.setTargetPackage(mygenEnv.getMapperPackage());
        javaClientGeneratorConfig.setConfigurationType("XMLMAPPER");
        javaClientGeneratorConfig.addProperty("enableSubPackages", "false");
        javaClientGeneratorConfig.addProperty("superInterface", mygenEnv.getSuperInterface());
        return javaClientGeneratorConfig;
    }

    @Bean
    public SqlMapGeneratorConfiguration sqlMapGeneratorConfig() {
        SqlMapGeneratorConfiguration sqlMapGeneratorConfig = new SqlMapGeneratorConfiguration();
        sqlMapGeneratorConfig.setTargetProject(mygenEnv.getResourcesPath());
        sqlMapGeneratorConfig.setTargetPackage(mygenEnv.getMapperXmlDir());
        sqlMapGeneratorConfig.addProperty("enableSubPackages", "false");
        return sqlMapGeneratorConfig;
    }

}
