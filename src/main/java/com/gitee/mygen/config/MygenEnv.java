package com.gitee.mygen.config;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Assert;
import com.gitee.mygen.impl.ModuleScanner;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "mygen")
public class MygenEnv implements InitializingBean {

    @Value("${spring.profiles.active}")
    private String profile;

    private String plugin;
    private String superInterface;
    private String mergeMode;
    private String generatePath;
    private String project;
    private String module;
    private String mapperDir;
    private List<String> tableNames;

    private ModuleScanner scanner;

    @Override
    public void afterPropertiesSet() {
        if (StringUtils.isBlank(superInterface) && "mybatis-plus".equals(plugin)) {
            superInterface = "com.baomidou.mybatisplus.core.mapper.BaseMapper";
        }

        Assert.isTrue("copy".equals(mergeMode) || "overwrite".equals(mergeMode),
                "The merge mode is not supported!");

        if ("user.home".equals(generatePath)) {
            String userHomeDir = System.getProperty("user.home");
            generatePath = userHomeDir + File.separator + "mygen" + File.separator + profile;
            FileUtil.mkdir(generatePath);
        }

        scanner = new ModuleScanner(project, project + module);
        scanner.scan("pojo", "mapper");
    }

    public String getSourcePath() {
        return generatePath;
    }

    public String getResourcesPath() {
        return generatePath;
    }

    public String getResourcesMapperPath() {
        return generatePath + File.separator + getMapperXmlDir();
    }

    public String getTargetSourcePath() {
        return scanner.getSourcePath();
    }

    public String getTargetResourcesPath() {
        return scanner.getResourcesPath();
    }

    public String getTargetResourcesMapperPath() {
        return scanner.getResourcesPath() + File.separator + getMapperXmlDir();
    }

    public String getPojoPackage() {
        return scanner.getPackage("pojo");
    }

    public String getMapperPackage() {
        return scanner.getPackage("mapper");
    }

    public String getMapperXmlDir() {
        return StringUtils.isNotBlank(mapperDir) ? mapperDir : "mapper";
    }

}
