package com.gitee.mygen.plugin.pojo;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.TopLevelClass;

import java.util.List;

public class JPAPlugin extends PluginAdapter {

    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }

    @Override
    public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        topLevelClass.addImportedType("javax.persistence.Table");
        topLevelClass.addAnnotation(String.format("@Table(name = \"%s\")", introspectedTable.getTableConfiguration().getTableName()));
        return true;
    }

    @Override
    public boolean modelFieldGenerated(Field field, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn,
                                       IntrospectedTable introspectedTable, ModelClassType modelClassType) {
        if ("id".equals(field.getName())) {
            topLevelClass.addImportedType("javax.persistence.Id");
            field.addAnnotation("@Id");
        } else {
            String columnName = introspectedColumn.getActualColumnName();
            if (columnName.contains("_")) {
                topLevelClass.addImportedType("javax.persistence.Column");
                field.addAnnotation(String.format("@Column(name = \"%s\")", columnName));
            }
        }
        return true;
    }

}
