package com.gitee.mygen.plugin.pojo;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.TopLevelClass;

import java.util.List;

public class MybatisPlusPlugin extends PluginAdapter {

    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }

    @Override
    public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        topLevelClass.addImportedType("com.baomidou.mybatisplus.annotation.TableName");
        topLevelClass.addAnnotation(String.format("@TableName(value = \"%s\")", introspectedTable.getTableConfiguration().getTableName()));
        return true;
    }

    @Override
    public boolean modelFieldGenerated(Field field, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn, IntrospectedTable introspectedTable,
                                       ModelClassType modelClassType) {
        if ("id".equals(field.getName())) {
            topLevelClass.addImportedType("com.baomidou.mybatisplus.annotation.TableId");
            topLevelClass.addImportedType("com.baomidou.mybatisplus.annotation.IdType");
            field.addAnnotation("@TableId(value = \"id\", type = IdType.AUTO)");
        } else {
            String columnName = introspectedColumn.getActualColumnName();
            if (columnName.contains("_")) {
                topLevelClass.addImportedType("com.baomidou.mybatisplus.annotation.TableField");
                field.addAnnotation(String.format("@TableField(value = \"%s\")", columnName));
            }
        }
        return true;
    }

}
