package com.gitee.mygen.plugin;

import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.internal.types.JavaTypeResolverDefaultImpl;

import java.sql.Types;

public class JavaTypeResolver extends JavaTypeResolverDefaultImpl {

    public JavaTypeResolver() {
        addMappingType(Integer.class, "TINYINT", Types.TINYINT);
    }

    private void addMappingType(Class<?> javaType, String jdbcTypeName, int jdbcTypeEnum) {
        FullyQualifiedJavaType fullyQualifiedJavaType = new FullyQualifiedJavaType(javaType.getName());
        JdbcTypeInformation jdbcTypeInformation = new JavaTypeResolverDefaultImpl.JdbcTypeInformation(jdbcTypeName, fullyQualifiedJavaType);
        super.typeMap.put(jdbcTypeEnum, jdbcTypeInformation);
    }

}
