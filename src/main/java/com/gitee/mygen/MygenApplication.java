package com.gitee.mygen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MygenApplication {
    public static void main(String[] args) {
        SpringApplication.run(MygenApplication.class, args);
    }
}
