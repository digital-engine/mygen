# mygen

mybatis-generator的拓展小工具，支持mybatis-plus和tk-mybatis两种逆向工程。

- 无需配置generatorConfig.xml。
- 生成的pojo支持lombok注解。
- 生成的xml非常简洁。

## 快速开始

1. 点击最新的发行版，下载并解压mygen.rar。

2. 修改application-def.yml配置文件：

   ```yml
   spring:
     datasource:
       url: jdbc:mysql://localhost:3306/db?characterEncoding=utf-8&useSSL=false&useUnicode=true&allowMultiQueries=true&zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=Asia/Shanghai
       username: your username
       password: your password
       driver-class-name: com.mysql.cj.jdbc.Driver
   
   mygen:
     plugin: mybatis-plus                                    # 插件类型（mybatis-plus、tk-mybatis）
     merge-mode: copy                                        # 合并方式（copy、overwrite）
     generate-path: user.home                                # 临时文件生成路径（user.home代表用户主目录）
     project: D:\xxxx\xxxx\project                           # 项目路径
     module: \xxxx\module                                    # 模块路径
     mapper-dir: mapper                                      # mapper.xml生成目录
     table-names: tableName1, tableName2                     # 表名（逗号分隔）
   ```
   
3. 双击start-def.bat，运行程序。
